from datetime import datetime
from json import JSONEncoder
from django.db.models import QuerySet


# this file holds our encoder code


class DateEncoder(JSONEncoder):
    def default(self, o):
        # if o is an instance of datetime
        if isinstance(o, datetime):
        #    return o.isoformat()
            return o.isoformat()
        # otherwise
        else:
        #    return super().default(o)
            return super().default(o)


# this is so we can change a queryset to a 
    # list in order to put it in JSONResponse
class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        # if o is an instance of a QuerySet, just turn
            # it into a normal list rather than the fancy
            # list that it already is.
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)





# JSONEncoder is a helper class
# supports everal data types (see doc)
# since JSONEncoder does NOT support classes, we need to write our own JSONEncoder
    # by overriding the default method
class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    # If you mean why we have to declare an empty encoders = {} 
        # in ModelEncoder class, then it is there in case some 
        # CustomEncoder we created in Views don't have a encoders 
        # variable (like LocationDetailEncoder - doesn't have encoders 
        # variable vs ConferenceDetailEncoder - have encoders variable) 
        # / So we need to create an empty encoders = {} otherwise 
        # the ModelEncoder won't be able to run
    encoders = {}
    
    # need to import this class into our views files
    # WHAT IS o???
    # print(o)
    # o is the specific item we are iterating over
    # so, in this case, it's the queryset of all conferences, then 
        # their specific names and anything related to that conference
    def default(self, o):
        #   if the object to decode is the same class as what's in the
        #   model property, then
        
        # self.model is the specific model from models.py to find instance of
        # print(self.model)
        # print("o:", o)
        if isinstance(o, self.model):
        #     * create an empty dictionary that will hold the property names
        #       as keys and the property values as values
            d = {}
        #     * for each name in the properties list
        #         * get the value of that property from the model instance
        #           given just the property name
        #         * put it into the dictionary with that property name as
        #           the key
        
            # if o has the attribute get_api_url
            #    then add its return value to the dictionary
            #    with the key "href"
            if hasattr(o, "get_api_url"):
                d["href"] = o.get_api_url()
        
            # self.properties comes from the ConferenceDetailEncoder 
                # properties field
            for property in self.properties:
                value = getattr(o, property)
                
                # print(o) # o gives us the name of the conference 
                #     # of that specific id
                # print(property) # gives us the specific thing we want
                    # ie "name", "description", etc
                # getattr() gets the property from the conference and 
                    # assigns it into value
                
                # print("self.encoders:", self.encoders)
                # checks to see if the property from ConferenceDetailEncoder (properties)
                    # is in self.encoders
                    # self.encoders is the "encoders = {}" below properties
                if property in self.encoders:
                    # since property matches the property in self.encoders
                        # it goes into this if statement
                    # print("property:", property)
                    
                    # print("self.encoders[property]:", self.encoders[property])
                    # this gives out a queryset:
                        # <events.api_views.LocationListEncoder object at 0x000001FC1A9F9450>
                
                    encoder = self.encoders[property]
                    
                    # this gives a dict of "href" and "name"
                        # {'href': '/api/locations/4/', 'name': 'Georgia World Congress Center'}
                    # encoder.default(value) goes back up to def default(self, o)
                        # and goes to LocationListEncoder to find the properties you want
                        # properties in LocationListEncoder is "name" + the "href"
                        # from above
                    # print("encoder.default(value):", encoder.default(value))
                    value = encoder.default(value)
                
                # goes to get_extra_data function, returns {}
                # print("get_extra_data:", self.get_extra_data(o))
                # updates d with {}
                d.update(self.get_extra_data(o))
                # print("d:", d)
                
                # puts it in a dict with property as key and value
                d[property] = value
                
        #     * return the dictionary
            return d
        #   otherwise,
        #       return super().default(o)  # From the documentation
        else:
            # If so, we run super().default(o) in order to kick the error 
            # (if there is an error) up to the parent's default() method in 
            # hopes that the parent's default() method can turn the object 
            # into something serializable. If it can't, then the chain of 
            # kicking the error up will continue until it hits the highest 
            # parent's (the JsonEncoder's) default() method, where it will
            # simply throw the error we all know and love: "such and 
            # such is not JSON serializable".
            return super().default(o)
        
        
    def get_extra_data(self, o):
        # this is just in case the views.py Encoders don't have a get_extra_data
        # doesn't throw an error
        return {}
        
           
