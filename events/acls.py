from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    url = 'https://api.pexels.com/v1/search'
    params = {
        "per_page": 1,
        "query": city + " " + state,
        }
  
    photo = requests.get(url, params=params, headers=headers)
    # this print statement gives us the JSON string
    # print(photo.content)
    
    # why use json.loads() instead of .json()
    # josh says there's no difference in using either
    content = json.loads(photo.content)
    return {"picture_url": content["photos"][0]["url"]}

def get_weather_data(city, state):
    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    lat_lon_url = 'http://api.openweathermap.org/geo/1.0/direct'
    params = {
        "q": city + "," + state + ",",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    
    response = requests.get(lat_lon_url, params=params, headers=headers)
    
    content = json.loads(response.content)
    lat = content[0]["lat"]
    lon = content[0]["lon"]
    
    params_for_weather = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
    }
    weather_url = 'https://api.openweathermap.org/data/2.5/weather'
    response = requests.get(weather_url, params=params_for_weather, headers=headers)
    
    content = json.loads(response.content)
    kelvin = content["main"]["temp"]
    fahrenheit = 9/5*(kelvin-273) + 32

    weather = {}
    weather["temperature"] = fahrenheit
    weather["description"] = content["weather"][0]["description"]
    return weather