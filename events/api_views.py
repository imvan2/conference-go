from weakref import WeakMethod
from django.http import JsonResponse
from common.json import ModelEncoder

from .models import Conference, Location, State

from django.views.decorators.http import require_http_methods
import json
from .acls import get_photo, get_weather_data

class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
    ]

@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    """
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    {
        "conferences": [
            {
                "name": conference's name,
                "href": URL to the conference,
            },
            ...
        ]
    }
    """
    
    # removing this so we can use the ConferenceListEncoder
    # response = []
    # conferences = Conference.objects.all()
    # for conference in conferences:
    #     response.append(
    #         {
    #             "name": conference.name,
    #             "href": conference.get_api_url(),
    #         }
    #     )
    # return JsonResponse({"conferences": response})
    
    if request.method == "GET":
        # Conference.objects.all() gets a queryset
        # queryset is just a fancy list tied to the data in the database
        # we need to turn the queryset into a list
        # so we need another encoder!
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        content = json.loads(request.body)
        
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
            
        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceListEncoder,
            safe=False,
        )

# need to create this LocationListEncoder cause 
    # type Location is not serializable
class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class ConferenceDetailEncoder(ModelEncoder):
    # inherits from ModelEncoder
    model = Conference
    
    # properties are the things we want to get
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts", # datetime is not JSON serializable
        "ends", # datetime is not JSON serializable
        "created", # datetime is not JSON serializable
        "updated", # datetime is not JSON serializable
        "location",
    ]
    # this is so key "location" refers to LocationListEncoder()
    # which goes to ModelEncoder
    encoders = {
        "location": LocationListEncoder(),
    }

def api_show_conference(request, pk):
    """
    Returns the details for the Conference model specified
    by the pk parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href.

    {
        "name": the conference's name,
        "starts": the date/time when the conference starts,
        "ends": the date/time when the conference ends,
        "description": the description of the conference,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "max_presentations": the maximum number of presentations,
        "max_attendees": the maximum number of attendees,
        "location": {
            "name": the name of the location,
            "href": the URL for the location,
        }
    }
    """
    # removing this so we can use ConferenceDetailEncoder
    # conference = Conference.objects.get(id=pk)
    # return JsonResponse(
    #     {
    #         "name": conference.name,
    #         "starts": conference.starts,
    #         "ends": conference.ends,
    #         "description": conference.description,
    #         "created": conference.created,
    #         "updated": conference.updated,
    #         "max_presentations": conference.max_presentations,
    #         "max_attendees": conference.max_attendees,
    #         "location": {
    #             "name": conference.location.name,
    #             "href": conference.location.get_api_url(),
    #         },
    #     }
    conference = Conference.objects.get(id=pk)
    weather = get_weather_data(conference.location.city, conference.location.state.abbreviation)
    
    return JsonResponse(
        # the data to send to JsonResponse
        {"conference": conference, "weather": weather},
        
        # the encoder to use
        encoder=ConferenceDetailEncoder,
        
        # need to set this because conference is NOT an accepted data type
        safe=False,
    )

#this allows only GET and POST requests to this function
@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.

    {
        "locations": [
            {
                "name": location's name,
                "href": URL to the location,
            },
            ...
        ]
    }
    """
    
    # hiding this to show location list using ModelEncoder
    # response = []
    # locations = Location.objects.all()
    # for location in locations:
    #     response.append(
    #         {
    #             "name": location.name,
    #             "href": location.get_api_url(),
    #         }
    #     )
    # return JsonResponse({"locations": response})
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )
    else: # if request.method is a "POST"
        # request.body is what you want to create (comes as a string)
        # json.loads() takes in a string and returns a json object
        # print(request.body)
        content = json.loads(request.body)
        # print(content)
        
        # 2 ways to solve the problem since state is a foreignkey in Location model:
        # 1. look up the state based on abbreviation in the JSON
        # 2. Send an id value that specifically identifies the State
        
        # Below is option 1
        try:
            # this gets the abbreviation of the state from State.objects
            state = State.objects.get(abbreviation=content["state"])
            
            # assigns the state to key state in content
            content["state"] = state
            # Use the city and state's abbreviation in the content dictionary
            # to call the get_photo ACL function
            photo = get_photo(content["city"], content["state"].abbreviation)
            content.update(photo)
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        
        
            
        # need to create the content using **kwargs (since content is a dict)
        # without it will create an error
        # QuerySet.create() only takes 1 positional argument
        location = Location.objects.create(**content)
        # print("location:", location)
        
        
        # Use the returned dictionary to update the content dictionary
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )

class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
        # "state", # get rid of this and add it to the bottom
    ]
    
    def get_extra_data(self, o):
        # print(o.state.abbreviation)
        return { "state": o.state.abbreviation }

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, pk):
    """
    Returns the details for the Location model specified
    by the pk parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }
    """
    # hiding for ModelEncoder
    # location = Location.objects.get(id=pk)
    # return JsonResponse({
    #         "name": location.name,
    #         "city": location.city,
    #         "room_count": location.room_count,
    #         "created": location.created,
    #         "updated": location.updated,
    #         "state": location.state.name,
    #     })
    
    if request.method == "GET":
        location = Location.objects.get(id=pk)
        
        # "state" is not JSON serializable
        # see ModelEncoder for the answer
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else: # if request.method is "PUT"
        content = json.loads(request.body)
        
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        Location.objects.filter(id=pk).update(**content)
        
        location = Location.objects.get(id=pk)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )